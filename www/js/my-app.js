var myApp = new Framework7({

  cache:false,
  modalTitle:"SrithaiGo",
  onAjaxStart: function (xhr) {
    myApp.showIndicator();
  },
  onAjaxComplete: function (xhr) {
    myApp.hideIndicator();
  }

});

var API_URL = {
  "token":"https://it.srithai-api.ei.team/api-token-auth/",
  "user-info": "https://it.srithai-api.ei.team/api/user-info/",
  "customer": "https://it.srithai-api.ei.team/api/customer/",
  "sku": "https://it.srithai-api.ei.team/api/sku/",
  "material": "https://it.srithai-api.ei.team/api/material/",
  "pcd": "https://it.srithai-api.ei.team/api/pcd/",
  "mcd": "https://it.srithai-api.ei.team/api/mcd/",
  "shopping": "https://it.srithai-api.ei.team/api/recieve-shop-list/",
  "recieve-shop-list": "https://it.srithai-api.ei.team/api/recieve-shop-list/",
  "recieve": "https://it.srithai-api.ei.team/api/recieve/",
  "classify": "https://it.srithai-api.ei.team/api/classify/",
  "scrap": "https://it.srithai-api.ei.team/api/scrap/",
  "promotion-info": "https://it.srithai-api.ei.team/api/promotion-info/",
  "promotion": "https://it.srithai-api.ei.team/api/promotion/",
  "promotion-create": "https://it.srithai-api.ei.team/api/promotion-create/",
  "nearly-expire-promotion": "https://it.srithai-api.ei.team/api/nearly-expire-promotion/",
  "pack": "https://it.srithai-api.ei.team/api/pack/",
  "pack-pcd": "https://it.srithai-api.ei.team/api/pack-pcd/",
  "order-retrieve": "https://it.srithai-api.ei.team/api/order-retrieve/",
  "incomplete-recieve": "https://it.srithai-api.ei.team/api/incomplete-recieve/",
  "claim": "https://it.srithai-api.ei.team/api/claim/",
  "claim-order": "https://it.srithai-api.ei.team/api/claim-order/",
  "invoice": "https://it.srithai-api.ei.team/api/invoice/",
  "material-type": "https://it.srithai-api.ei.team/api/material-type/",
  "material-cost-create": "https://it.srithai-api.ei.team/api/material-cost-create/",
  "material-cost-retrieve": "https://it.srithai-api.ei.team/api/material-cost-retrieve/",
  "rework": "https://it.srithai-api.ei.team/api/rework/",
  "rework-sku": "https://it.srithai-api.ei.team/api/rework-sku/",
  "rework-mt": "https://it.srithai-api.ei.team/api/rework-mt/",
  "brief-all-order": "https://it.srithai-api.ei.team/api/brief-all-order/",
  "customer-received":"https://it.srithai-api.ei.team/api/customer-received/"
};

var $$ = Dom7;

var mainView = myApp.addView('.view-main', {
  dynamicNavbar: true
});

login_check();

$$('.login-button').on('click', function () {

  var username = $$('.login-screen [name="username"]').val();
  var password = $$('.login-screen [name="password"]').val();


  if(username == ''){
    myApp.alert('ไม่ได้ระบบชื่อผู้ใช้งานระบบ');
  }else if(password == ''){
    myApp.alert('ไม่ได้ระบุรหัสผ่าน');
  }else{

    login_todo(username,password)

  }

});

myApp.onPageBack('index', function(){
  location.reload()
})


myApp.onPageInit('index', function (page) {
 myApp.closeModal('.login-screen',true);

 var login_app = myApp.formGetData('login');
 $$('.namecustomer').html(login_app.detail.first_name);
 var pageel = $$(page.container);

 $$(pageel).find('.goto_barcode_fill').on('click',function(){
  mainView.router.loadPage('page/getin.html');
});

 $$(pageel).find('.goto_document_check_fill').on('click',function(){
  mainView.router.loadPage('page/getstatus.html');
});

 $$(pageel).find('.goto_time_fill').on('click',function(){
  mainView.router.loadPage('page/getstatusm.html');
});

 $$(pageel).find('.goto_alert_fill').on('click',function(){
  mainView.router.loadPage('page/goto_alert_fill.html');
});


 $$(pageel).find('.goto_exit_fill').on('click',function(){
  myApp.formDeleteData('login');
  mainView.back();
});

});

myApp.onPageInit('getin',function(page){
  var pageel = $$(page.container);
  var data1;
  var data2;
  myApp.showIndicator();
  var html = '';
  $$.ajax({
    url:API_URL['customer-received'],
    method:'GET',
    cache:true,
    contentType:'application/json',
    crossDomain:true,
    dataType:'json',
    error:function(data,status){
      if(data.status == 401){
        myApp.alert('เกิดข้อผิดพลาดบางอย่าง');
      }
    },
    success:function(data,status,xhr){
      console.log(data.results)
      data1 = data.results;

      $$.each(data.results, function (index1, value1) {
       var d = new Date(value1.created_datetime);

        $$.ajax({

        url:API_URL['invoice']+value1.invoice,
        method:'GET',
        cache:true,
        contentType:'application/json',
        crossDomain:true,
        dataType:'json',

        error:function(data,status){
          if(data.status == 401){
            myApp.alert('เกิดข้อผิดพลาดบางอย่าง');
          }
        },

        success:function(data,status,xhr){
         console.log(data)
         data2 = data.items;
       }

     });

       html += '<div class="content-block-title">('+value1.invoice_name+') ส่งสินค้าเมื่อ <strong class="color-srithai">' + dayNames[d.getDay()]+'  '+d.getDate()+' '+monthNamesThai[d.getMonth()]+'  '+d.getFullYear()+'</strong></div>';

       // html += '<div class="content-block">';
       // html += '<div class="row">';
       // html += '<div class="col-33">';
       // html += '<div class="bg-getin-01 bg-getin">';
       // html += 'จำนวน สั่ง<br>';
       // html += '0';
       // html += '</div>';
       // html += '</div>';
       // html += '<div class="col-33">';
       // html += '<div class="bg-getin-02 bg-getin">';
       // html += 'จำนวน รับ<br>';
       // html += '0';
       // html += '</div>';
       // html += '</div>';
       // html += '<div class="col-33">';
       // html += '<div class="bg-getin-03 bg-getin">';
       // html += 'จำนวน เหลือ<br>';
       // html += '0';
       // html += '</div>';
       // html += '</div>';
       // html += '</div>';
       // html += '</div>';

       html += '<div class="data-table data-table-init card">';
       html += '<table>';
       html += '<thead>';
       html += '<tr>';
       html += '<th class="checkbox-cell">';
       html += '<label class="form-checkbox">';
       html += '<input type="checkbox"/><i></i>';
       html += '</label>';
       html += '</th>';
       html += '<th class="label-cell">ชื่อ</th>';
       html += '<th class="label-cell">สั่ง</th>';
       html += '<th class="label-cell">ส่ง</th>';
       html += '<th class="label-cell">ประเภท</th>';
       html += '</tr>';
       html += '</thead>';
       html += '<tbody>';
       $$.each(value1.items, function (index2, value2) {

         html += '<tr>';
         html += '<td class="checkbox-cell">';
         html += '<label class="form-checkbox">';
         html += '<input type="checkbox"/><i></i>';
         html += '</label>';
         html += '</td>';
         html += '<td class="label-cell">'+value2.sku.title+'</td>';
         html += '<td class="numeric-cell">'+value2.quantity+'</td>';
         html += '<td class="numeric-cell id_'+value2.id+'">Check..</td>';
         html += '<td class="label-cell">'+value2.sku.unit+'</td>';
         html += '</tr>';

       });

       html += '</tbody>';
       html += '</table>';
       html += '</div>';
     });

      $$(pageel).find('.page-content').html(html);


    },
    complete:function(){
     myApp.hideIndicator();
   }
 });
})

myApp.onPageInit('getstatus',function(page){
  var pageel = $$(page.container);
  myApp.showIndicator();
  var html = '';
  $$.ajax({
    url:API_URL['order-retrieve'],
    method:'GET',
    cache:true,
    contentType:'application/json',
    crossDomain:true,
    dataType:'json',
    error:function(data,status){
      if(data.status == 401){
        myApp.alert('เกิดข้อผิดพลาดบางอย่าง');
      }
    },
    success:function(data,status,xhr){
      console.log(data)
      $$.each(data, function (index1, value1) {
       var d = new Date(value1.created_datetime);

       html += '<div class="content-block-title">('+value1.invoice_name+') ส่งสินค้าเมื่อ <strong class="color-srithai">' + dayNames[d.getDay()]+'  '+d.getDate()+' '+monthNamesThai[d.getMonth()]+'  '+d.getFullYear()+'</strong></div>';
       html += '<div class="data-table card">';
       html += '<table>';
       html += '<thead>';
       html += '<tr>';
       html += '<th class="label-cell">ชื่อ</th>';
       html += '<th class="label-cell">จำนวน</th>';
       html += '<th class="label-cell">ราคา</th>';
       html += '<th class="label-cell">รวม</th>';
       html += '<th class="label-cell">ประเภท</th>';
       html += '</tr>';
       html += '</thead>';
       html += '<tbody>';
       $$.each(value1.items, function (index2, value2) {
         html += '<tr>';
         html += '<td class="label-cell">'+value2.sku.title+'</td>';
         html += '<td class="numeric-cell">'+value2.quantity+'</td>';
         html += '<td class="numeric-cell">'+value2.price_per_unit+'</td>';
         html += '<td class="numeric-cell">'+value2.price_per_unit*value2.quantity+'</td>';
         html += '<td class="label-cell">'+value2.sku.unit+'</td>';
         html += '</tr>';
       });
       html += '</tbody>';
       html += '</table>';
       html += '</div>';
     });

      $$(pageel).find('.page-content').html(html);
    },
    complete:function(){
     myApp.hideIndicator();
   }
 });
})