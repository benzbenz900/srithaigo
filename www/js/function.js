var login_check = function(){
  var login_app = myApp.formGetData('login');
  myApp.showIndicator();
  setTimeout(function(){
    if(login_app){
        login_todo(login_app.username,login_app.password);
    }else{
      myApp.loginScreen('.login-screen',true);
  }
  myApp.hideIndicator();
},1000);
}

var login_todo = function(username,password){
    var tok = username + ':' + password;
    var hash = btoa(tok);
    var Basic = "Basic " + hash;

    $$.ajaxSetup({
        headers: {
            'Authorization': Basic
        }
    });

    var form = new FormData();
    form.append("username", username);
    form.append("password", password);

    $$.ajax({
        "async": true,
        "crossDomain": true,
        "url":API_URL['token'],
        "method": "POST",
        "processData": false,
        "contentType": false,
        "mimeType": "multipart/form-data",
        "data": form,
        error:function(data,status){
            if(data.status == 401){
                myApp.alert('ชื่อผู้ใช้ หรือ รหัสผ่านผิด กรุณาลองใหม่อีกครั้ง');
            }
        },
        success:function(data,status,xhr){
            data =JSON.parse(data);
            $$.ajax({
              "async": true,
              "crossDomain": true,
              "url": "https://it.srithai-api.ei.team/api/user/",
              "method": "GET",
              "headers": {
                "Authorization": "Token "+data.token
            },
            success:function(data,status,xhr){
                myApp.formStoreData('login', {'username':username,'password':password,'detail':JSON.parse(data)});
                mainView.router.loadPage('page/index.html');
            }
        });
        }
    });

}

var monthNamesThai = ["มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน",
"กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤษจิกายน","ธันวาคม"];

var dayNames = ["วันอาทิตย์ที่","วันจันทร์ที่","วันอังคารที่","วันพุทธที่","วันพฤหัสบดีที่","วันศุกร์ที่","วันเสาร์ที่"];
